Rails.application.routes.draw do
  
  devise_for :users

  get '/admin', to: 'admin/home#panel', as: :admin_root
  root 'admin/home#panel'


  namespace :admin do 
  	resources :roles
  	resources :sales
  	resources :clients, except: [:index]
  	resources :entries
  	resources :providers
  	resources :warehouses
  	resources :expenses
  	resources :settings, except: [:index, :show, :new, :create, :destroy]
  	
    resources :products do 
    	collection { post :import}
    end
  end

  scope '/admin' do 
		get 'users', to: 'admin/user#landing_users', as: :users_landing
		scope '/users' do 
			get 'list', to: 'admin/user#index_users', as: :users_list
			get 'web-list', to: 'admin/user#web_users', as: :users_web
			get 'new', to: 'admin/user#new', as: :user_new
			get ':id/edit', to: 'admin/user#edit', as: :user_edit

			post '', to: 'admin/user#create', as: :user_create
			put ':id', to: 'admin/user#update', as: :user_update
			patch ':id', to: 'admin/user#update'
			delete ':id', to: 'admin/user#destroy', as: :user_destroy
		end

		get 'clients', to: 'admin/clients#landing_clients', as: :clients_landing
		get 'clients-index', to: 'admin/clients#index', as: :clients_list

		get 'stock-landing', to: 'admin/products#landing', as: :products_landing

		get 'expenses-landing', to: 'admin/pages#landing_expenses', as: :expenses_landing
		get 'sales-landing', to: 'admin/pages#landing_sales', as: :sales_landing

		get 'sales-por-pagar', to: 'admin/sales#por_pagar', as: :sales_por_pagar
		get 'sales-cotizaciones', to: 'admin/sales#cotizaciones', as: :sales_cotizaciones
		get 'sales-parcialmente-pagadas', to: 'admin/sales#parcialmente_pagadas', as: :sales_parcialmente_pagadas
		get 'sales-pagadas', to: 'admin/sales#pagadas', as: :sales_pagadas

		# expenses 
		get 'list-buy', to: 'admin/expenses#index_buy', as: :index_buy

		get 'show-buy/:id', to: 'admin/expenses#show_buy', as: :show_buy
		get 'new-buy', to: 'admin/expenses#new_buy', as: :new_buy
		get 'edit-buy/:id', to: 'admin/expenses#edit_buy', as: :edit_buy
		post 'create-buy', to: 'admin/expenses#create_buy', as: :create_buy
		put 'edit-buy/:id', to: 'admin/expenses#update_buy', as: :update_buy
		patch 'edit-buy/:id', to: 'admin/expenses#update_buy'

		get 'list-nominas', to: 'admin/expenses#index_nomina', as: :index_nomina
		get 'show-nomina/:id', to: 'admin/expenses#show_nomina', as: :show_nomina
		get 'new-nomina', to: 'admin/expenses#new_nomina', as: :new_nomina
		get 'edit-nomina/:id', to: 'admin/expenses#edit_nomina', as: :edit_nomina
		post 'create-nomina', to: 'admin/expenses#create_nomina', as: :create_nomina
		put 'edit-nomina/:id', to: 'admin/expenses#update_nomina', as: :update_nomina
		patch 'edit-nomina/:id', to: 'admin/expenses#update_nomina'


		# product out
		get 'list-exits', to: 'admin/entries#index_out', as: :list_exits
		get 'list-exit/:id', to: 'admin/entries#show_out', as: :show_exit
		get 'product-out', to: 'admin/entries#product_out', as: :new_out
		get 'edit-out/:id', to: 'admin/entries#edit_out', as: :edit_out
		post '', to: 'admin/entries#create_out', as: :create_out
		put 'edit-out/:id', to: 'admin/entries#update_out', as: :update_out
		patch 'edit-out/:id', to: 'admin/entries#update_out'

		get 'reports', to: 'admin/pages#reports', as: :reports

	end
	
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
