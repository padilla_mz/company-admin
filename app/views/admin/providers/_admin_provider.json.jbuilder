json.extract! admin_provider, :id, :created_at, :updated_at
json.url admin_provider_url(admin_provider, format: :json)