json.extract! admin_warehouse, :id, :created_at, :updated_at
json.url admin_warehouse_url(admin_warehouse, format: :json)