json.extract! client, :id, :created_at, :updated_at
json.url admin_client_url(client, format: :json)