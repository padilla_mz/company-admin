json.extract! admin_entry, :id, :created_at, :updated_at
json.url admin_entry_url(admin_entry, format: :json)