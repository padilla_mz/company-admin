json.extract! admin_sale, :id, :created_at, :updated_at
json.url admin_sale_url(admin_sale, format: :json)