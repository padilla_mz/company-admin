class Admin::ProvidersController < ApplicationController
  before_action :check_admin
  before_action :set_admin_provider, only: [:show, :edit, :update, :destroy]
  layout 'admin'

  # GET /admin/providers
  # GET /admin/providers.json
  def index
    @q = Provider.ransack(params[:q])
    @admin_providers = @q.result(distinct: true).order(razon: :desc)
  end

  # GET /admin/providers/1
  # GET /admin/providers/1.json
  def show
  end

  # GET /admin/providers/new
  def new
    @admin_provider = Provider.new
  end

  # GET /admin/providers/1/edit
  def edit
  end

  # POST /admin/providers
  # POST /admin/providers.json
  def create
    @admin_provider = Provider.new(admin_provider_params)

    respond_to do |format|
      if @admin_provider.save
        format.html { redirect_to admin_providers_path, notice: 'Provider was successfully created.' }
        format.json { render :show, status: :created, location: admin_providers_path }
      else
        format.html { render :new }
        format.json { render json: @admin_provider.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/providers/1
  # PATCH/PUT /admin/providers/1.json
  def update
    respond_to do |format|
      if @admin_provider.update(admin_provider_params)
        format.html { redirect_to admin_providers_path, notice: 'Provider was successfully updated.' }
        format.json { render :show, status: :ok, location: admin_providers_path }
      else
        format.html { render :edit }
        format.json { render json: @admin_provider.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/providers/1
  # DELETE /admin/providers/1.json
  def destroy
    @admin_provider.destroy
    respond_to do |format|
      format.html { redirect_to admin_providers_url, notice: 'Provider was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def check_admin
      unless current_user.has_roles?([:super, :admin, :gerente_inventario, :compras, :gerente_compras])
        redirect_to admin_root_path, notice: "Error 9001 no tienes permiso"
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_provider
      @admin_provider = Provider.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_provider_params
      params.require(:provider).permit(:razon, :rfc, :address, :cp, :city, :localidad, :ext_number, :int_number, :phone, :email, :state, :col, :comment)
    end
end
