class Admin::PagesController < ApplicationController
	layout 'admin'

	def landing_expenses
		unless current_user.has_roles? [:super, :admin, :compras, :gerente_compras]
			redirect_to admin_root_path, notice: "Error 9001 no tienes permiso"
		end
  	@this_month = Expense.last_month_sum
		
	end
	
	def landing_sales
		unless current_user.has_roles? [:super, :admin, :ventas, :gerente_ventas]
			redirect_to admin_root_path, notice: "Error 9001 no tienes permiso"
		end
	end

	def reports
		unless current_user.has_roles? [:super, :admin, :ventas, :gerente_ventas, :compras, :gerente_compras, :inventario, :gerente_inventario]
			redirect_to admin_root_path, notice: "Error 9001 no tienes permiso"
		end
		@days = Time.days_in_month(Time.now.month)
		
		@compras = Expense.last_month_sum
		@ventas = Deposit.last_month_sum

		@sales_list = Deposit.last_month
		@purchases_list = Expense.last_month
		
		
	end
		

end
