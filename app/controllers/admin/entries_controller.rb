class Admin::EntriesController < ApplicationController
  before_action :check_admin
  before_action :set_entry, only: [:show, :show_out, :edit, :edit_out, :update, :destroy, :update_out]
  layout 'admin'

  # GET /admin/entries
  # GET /admin/entries.json
  def index
    @q = Entry.ransack(params[:q])
    @admin_entries = @q.result(distinct: true).ins
  end

  def index_out
    @q = Entry.ransack(params[:q])
    @admin_entries = @q.result(distinct: true).outs
  end

  # GET /admin/entries/1
  # GET /admin/entries/1.json
  def show
  end
  def show_out
  end

  # GET /admin/entries/new
  def new
    @admin_entry = Entry.new
    @admin_entry.entries_products.build
    @payment = Entry::Payment_forms
    @bill_state = Entry::State_of_bills
    @tax = Entry::Tax
  end

  # GET
  def product_out
    @entry = Entry.new
    @entry.out_lines.build
  end

  # GET /admin/entries/1/edit
  def edit
    @payment = Entry::Payment_forms
    @bill_state = Entry::State_of_bills
    @tax = Entry::Tax
  end

  def edit_out
    
  end

  # POST /admin/entries
  # POST /admin/entries.json
  #  entrie 
  def create
    @entry = Entry.new(entry_params)
    @entry.entries_products.each do |entry_line|
      entry_line.total_price_line = entry_line.price * entry_line.quantity
      entry_line.product.quantity += entry_line.quantity
      entry_line.product.save
    end
    @entry.user_id = current_user.id
    @entry.direction = 'in'
    @entry.save
    @entry.subtotal = @entry.entries_products.sum(:total_price_line)

    if @entry.tax == 0
      @entry.total = @entry.subtotal
    else
      @entry.total = (@entry.subtotal) * (@entry.tax + 1)
    end


    respond_to do |format|
      if @entry.save
        format.html { redirect_to admin_entries_path, notice: 'Entry was successfully created.' }
        # format.json { render :show, status: :created, location: admin_entries_path }
      else
        format.html { render :new }
        # format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST 
  # out
  def create_out
    @entry = Entry.new(out_params)
    @entry.user_id = current_user.id
    @entry.direction = 'out'
    respond_to do |format|
      if @entry.save
        format.html { redirect_to list_exits_path, notice: 'La salida se ha creado con éxito!' }
        # format.json { render :show, status: :created, location: admin_entries_path }
      else
        format.html { render :new }
        # format.json { render json: @entry.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_out
    respond_to do |f|
      if @entry.update(out_params)
        f.html { redirect_to list_exits_path, notice: 'La salida se ha editado con éxito!'}
      else
        f.html {render :product_out}
      end
    end
    
  end



  # PATCH/PUT /admin/entries/1
  # PATCH/PUT /admin/entries/1.json
  def update
    @entry.assign_attributes(entry_params)

    @entry.entries_products.each do |entry_line|
      entry_line.total_price_line = entry_line.price * entry_line.quantity
      entry_line.product.quantity += entry_line.quantity
      entry_line.product.save 
    end
    @entry.save

    @entry.subtotal = @entry.entries_products.sum(:total_price_line)

    if @entry.tax == 0
      @entry.total = @entry.subtotal
    else
      @entry.total = (@entry.subtotal) * (@entry.tax + 1)
    end
    respond_to do |format|
      if @entry.save
        format.html { redirect_to admin_entries_path, notice: 'Entry was successfully updated.' }
        # format.json { render :show, status: :ok, location: admin_entries_path }
      else
        format.html { render :edit }
        # format.json { render json: @admin_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/entries/1
  # DELETE /admin/entries/1.json
  def destroy
    @entry.destroy
    respond_to do |format|
      format.html { redirect_to admin_entries_url, notice: 'Entry was successfully destroyed.' }
      # format.json { head :no_content }
    end
  end

  private
    def check_admin
      unless current_user.has_roles?([:super, :admin, :inventario, :gerente_inventario])
        redirect_to admin_root_path, notice: "Error 9001 no tienes permiso"
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_entry
      @entry = Entry.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def entry_params
      params.require(:entry).permit(:provider_id, :warehouse_id, :comment, :bill_number, :ticket_number, :subtotal, :tax, :total, :payment_form, :state_of_bill, :received_at, :payment_date, entries_products_attributes: [:quantity, :entry_sku, :price, :model, :product_id, :_destroy, :id])
    end
    def out_params
      params.require(:entry).permit(:client_id, out_lines_attributes: [:quantity, :model, :product_id, :_destroy, :id])
    end
end
