class Admin::RolesController < ApplicationController
	# roles who aply here :super	
	before_action :check_admin
  before_action :set_role, only: [:show, :edit, :update, :destroy]
	layout 'admin'



	# get requests
	def index
		@roles = Role.roles_index
	end

	def show
	end

	def new
		@role = Role.new
	end

	def edit
		
	end

	# post
	def create
		@role = Role.new(role_params)
		respond_to do |f|
      if @role.save
        f.html { redirect_to admin_roles_path, notice: 'Rol creado' }
      else
        f.html { render :new }
      end
    end
	end

	# patch/put
	def update
		respond_to do |f|
			if @role.update(role_params)
				f.html {redirect_to admin_roles_path, notice: "Rol actualizado"}
			else
				f.html {render :edit}
			end
		end
	end

	# delete
	def destroy
		@role.destroy
		respond_to do |f|
			f. html {redirect_to admin_roles_path, notice: "Rol eliminado"}
		end
	end
	private
		def check_admin
			unless current_user.has_role?(:super)
				redirect_to admin_root_path, notice: "Error 9001 no tienes permiso"
			end
		end

    def set_role
    	@role = Role.find(params[:id])
    end
    def role_params
    	params.require(:role).permit(:name, :comment)
    end


end









