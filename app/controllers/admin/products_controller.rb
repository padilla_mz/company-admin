class Admin::ProductsController < ApplicationController
  # role who apply here :super, :admin, :inventario, :gerente_inventario
  before_action :check_admin
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  layout 'admin'

  def landing
  end

  def import
    Product.import(params[:file])
    redirect_to admin_products_path, notice: "Productos importados!"
  end

  # GET /admin/products
  # GET /admin/products.json
  # GET /admin/products.csv
  def index
    @q = Product.ransack(params[:q])
    @products = @q.result(distinct: true)
    respond_to do |f|
      f.html
      f.csv 
      f.pdf do
        pdf = ProductosPdf.new(@products)
        send_data(pdf.render, filename: "products_#{Time.now}.pdf", type: "application/pdf", disposition: "inline")
      end
    end
  end


  # GET /admin/products/1
  # GET /admin/products/1.json
  def show
  end

  # GET /admin/products/new
  def new
    @product = Product.new
    @product_types = Product::Tipos
    @unidades = Product::Unidades
  end

  # GET /admin/products/1/edit
  def edit
    @product_types = Product::Tipos
    @unidades = Product::Unidades

  end

  # POST /admin/products
  # POST /admin/products.json
  def create
    # @product_types = Product::Tipos

    @product = Product.new(product_params)
    @product.brand = @product.brand.upcase
    @product.model = @product.model.upcase
    @product.sku = @product.sku.upcase
    
    # @product.sku = "#{@product.brand[0..2]}#{@product.model[0..3]}#{@product.ancho}-#{@product.alto}-#{@product.rin}"
    
    # precio medio mayoreo
    @product.price_1 = (@product.public_price * 0.95)

    # precio mayoreo
    @product.price_2 = (@product.public_price * 0.93)

    respond_to do |format|
      if @product.save
        format.html { redirect_to admin_product_path(@product), id: @product.id, notice: 'Producto fue creado' }
        # format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        # format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/products/1
  # PATCH/PUT /admin/products/1.json
  def update
    @product.assign_attributes(product_params)
    @product.brand = @product.brand.upcase
    @product.model = @product.model.upcase
    @product.sku = @product.sku.upcase
    # @product.sku = "#{@product.brand[0..2]}#{@product.model[0..3]}#{@product.ancho}-#{@product.alto}-#{@product.rin}"
    
    # @product.save
    # precio medio mayoreo
    @product.price_1 = (@product.public_price * 0.95)
    # precio mayoreo
    @product.price_2 = (@product.public_price * 0.93)

    respond_to do |format|
      if @product.save
        format.html { redirect_to admin_product_path(@product), notice: 'Producto fue actualizado' }
        # format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        # format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/products/1
  # DELETE /admin/products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to admin_products_url, notice: 'Producto fue destruido' }
      # format.json { head :no_content }
    end
  end

  private
    def check_admin
      unless current_user.has_roles?([:super, :admin, :gerente_inventario, :inventario])
        redirect_to admin_root_path, notice: "Error 9001 no tienes permiso"
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:capacity, :unidad, :cost, :product_type, :sku, :brand, :model, :ancho, :alto, :rin, :treadwear, :desc, :ind_carga, :ind_velocidad, :public_price, :price_2, :price_1)
    end
end
