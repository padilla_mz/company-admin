class Admin::HomeController < ApplicationController
	# all roles but web_user
	before_action :check_admin

	layout 'admin'
	
  # GET
  def panel
  	@company = Setting.first 
  	@img = @setting.img_64

  end

  private
  	def check_admin
			unless current_user.has_any_role?
				redirect_to root_path, notice: "Error 9001"
			end
		end
  
end
