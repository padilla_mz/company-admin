class Admin::ClientsController < ApplicationController
  # roles who aply here :super  
  before_action :check_admin
  before_action :set_client, only: [:show, :edit, :update, :destroy]
  layout 'admin'

  def landing_clients
    
  end

  # GET /admin/clients
  # GET /admin/clients.json
  def index
    @q = Client.ransack(params[:q])
    @clients = @q.result(distinct: true).order(name: :desc)
  end

  # GET /admin/clients/1
  # GET /admin/clients/1.json
  def show
  end

  # GET /admin/clients/new
  def new
    @client = Client.new
    @limit = Client::Limite
  end

  # GET /admin/clients/1/edit
  def edit
    @limit = Client::Limite

  end

  # POST /admin/clients
  # POST /admin/clients.json
  def create
    @client = Client.new(client_params)

    respond_to do |format|
      if @client.save
        format.html { redirect_to clients_list_path, notice: 'Client was successfully created.' }
        # format.json { render :show, status: :created, location: @client }
      else
        format.html { render :new }
        # format.json { render json: @admin_client.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/clients/1
  # PATCH/PUT /admin/clients/1.json
  def update
    respond_to do |format|
      if @client.update(client_params)
        format.html { redirect_to clients_list_path, notice: 'Client was successfully updated.' }
        # format.json { render :show, status: :ok, location: @client }
      else
        format.html { render :edit }
        # format.json { render json: @client.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/clients/1
  # DELETE /admin/clients/1.json
  def destroy
    @client.destroy
    respond_to do |format|
      format.html { redirect_to clients_list_path, notice: 'Client was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def check_admin
      unless current_user.has_roles?([:super, :admin, :ventas, :gerente_ventas])
        redirect_to admin_root_path, notice: "Error 9001 no tienes permiso"
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = Client.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_params
      params.require(:client).permit(:razon, :name, :rfc, :address, :col, :cp, :state, :city, :localidad, :phone_1, :phone_2, :ext_number, :int_number, :email, :credit_limit, :tipo_venta)

    end
end
