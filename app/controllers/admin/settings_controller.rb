class Admin::SettingsController < ApplicationController
  before_action :check_admin
  before_action :set_setting, only: [:show, :edit, :update, :destroy]
  layout 'admin'

  # GET /admin/settings
  # GET /admin/settings.json
  def index
    @settings = Setting.all
  end

  # GET /admin/settings/1
  # GET /admin/settings/1.json
  def show
  end

  # GET /admin/settings/new
  def new
    @setting = Setting.new
  end

  # GET /admin/settings/1/edit
  def edit
  end

  # POST /admin/settings
  # POST /admin/settings.json
  def create
    @setting = Setting.new(setting_params)

    respond_to do |format|
      if @setting.save
        format.html { redirect_to @setting, notice: 'Setting was successfully created.' }
        # format.json { render :show, status: :created, location: @admin_setting }
      else
        format.html { render :new }
        # format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/settings/1
  # PATCH/PUT /admin/settings/1.json
  def update
    respond_to do |format|
      if @setting.update(setting_params)
        format.html { redirect_to edit_admin_setting_path(@setting), notice: 'Setting was successfully updated.' }
        # format.json { render :show, status: :ok, location: @setting }
      else
        format.html { render :edit }
        # format.json { render json: @setting.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/settings/1
  # DELETE /admin/settings/1.json
  def destroy
    @setting.destroy
    respond_to do |format|
      format.html { redirect_to admin_settings_url, notice: 'Setting was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  def check_admin
    unless current_user.has_roles?([:super, :admin])
      redirect_to admin_root_path, notice: "Error 9001 no tienes permiso"
    end
  end
    # Use callbacks to share common setup or constraints between actions.
    def set_setting
      @setting = Setting.first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def setting_params
      params.require(:setting).permit(:img_64, :name, :phone_1, :phone_2, :email_1, :email_2, :web_page, :address, :state, :city, :cp, :razon, :rfc)
    end
end
