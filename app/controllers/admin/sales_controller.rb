class Admin::SalesController < ApplicationController
  before_action :check_admin
  before_action :set_sale, only: [:show, :edit, :update, :destroy]
  layout 'admin'

  # GET /admin/sales
  # GET /admin/sales.json


  def index
    @q = Sale.ransack(params[:q])
    @sales = @q.result(distinct: true).default
    respond_to do |f|
      f.html
      f.csv {headers['Content-Disposition'] = "attachment; filename=ventas_#{Time.now.strftime('%m-%d-%Y')}.csv"}
    end
  end


  def por_pagar
    @q = Sale.ransack(params[:q])
    @sales = @q.result(distinct: true).por_pagar
  end
 
  def cotizaciones
    @q = Sale.ransack(params[:q])
    @sales = @q.result(distinct: true).cotizaciones
    respond_to do |f|
      f.html
      f.csv {headers['Content-Disposition'] = "attachment; filename=cotizaciones_#{Time.now.strftime('%m-%d-%Y')}.csv"}
    end
  end

  def parcialmente_pagadas
    @q = Sale.ransack(params[:q])
    @sales = @q.result(distinct: true).parcialmente_pagadas
  end
  def pagadas
    @q = Sale.ransack(params[:q])
    @sales = @q.result(distinct: true).pagadas
  end

  # GET /admin/sales/1
  # GET /admin/sales/1.json
  def show
  end

  # GET /admin/sales/new
  def new

    @sale = Sale.new
    @sale.line_items.build
    @payments = Sale::Payment_forms

    @states_sales = Sale::State_of_sale
    @states_sales_1 = Sale::State_of_sale_1
    @states_sales_2 = Sale::State_of_sale_2
    @states_sales_3 = Sale::State_of_sale_3
    @states_sales_4 = Sale::State_of_sale_4

    @tax = Sale::Tax
    @discount = Sale::Discount
  end

  # GET /admin/sales/1/edit
  def edit
    @payments = Sale::Payment_forms
    # @sale.deposits.build
    @states_sales = Sale::State_of_sale
    @states_sales_1 = Sale::State_of_sale_1
    @states_sales_2 = Sale::State_of_sale_2
    @states_sales_3 = Sale::State_of_sale_3
    @states_sales_4 = Sale::State_of_sale_4

    @tax = Sale::Tax
    @discount = Sale::Discount




  end

  # POST /admin/sales
  # POST /admin/sales.json
  def create
    @sale = Sale.new(sale_params)
    @sale.line_items.each do |line|
      line.total_price_line = line.price * line.quantity
      line.save
    end
    @sale.user_id = current_user.id
    @sale.save

    date = @sale.created_at
    @sale.sku = "#{@sale.id}-#{date.month}#{date.day}#{date.year}"
    @sale.subtotal = @sale.line_items.sum(:total_price_line)

    if @sale.discount == 0
      @after_discount = @sale.subtotal
    else
      @after_discount = (@sale.subtotal) - (@sale.subtotal * @sale.discount)
    end

    if @sale.tax == 0
      @sale.total = @after_discount
    else
      @sale.total = (@after_discount) * (@sale.tax + 1)
    end

    if @sale.deposits.count > 0
      @sale.payment_status = 'parcialmente_pagado'
    end
    @sale.accumulated = @sale.deposits.sum(:amount)
    @sale.remaining = @sale.total - @sale.accumulated

    if @sale.payment_status == 'venta_concretada'
      @sale.accumulated = @sale.total
      Deposit.create(amount: @sale.remaining, description: "Pago en una sola exhibición", sale_id: @sale.id)
    end
    if @sale.accumulated >= @sale.total 
      @sale.payment_status = 'venta_concretada'
    end


    respond_to do |format|
      if @sale.save
        format.html { redirect_to admin_sale_path(@sale), notice: 'Sale was successfully created.' }
        # format.json { render :show, status: :created, location: @sale }
      else
        format.html { render :new }
        # format.json { render json: @sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/sales/1
  # PATCH/PUT /admin/sales/1.json
  def update
    @sale.assign_attributes(sale_params)
    @sale.line_items.each do |line|
      line.total_price_line = line.price * line.quantity
      line.save
    end
    # if @sale.deposits
    #   @sale.deposits.each do |dep|
    #     line.total_price_line = line.price * line.quantity
    #     line.save
    #   end
    # end
    @sale.user_id = current_user.id
    @sale.save
    @sale.subtotal = @sale.line_items.sum(:total_price_line)


    if @sale.discount == 0
      @after_discount = @sale.subtotal
    else
      @after_discount = (@sale.subtotal) - (@sale.subtotal * @sale.discount)
    end

    if @sale.tax == 0
      @sale.total = @after_discount
    else
      @sale.total = (@after_discount) * (@sale.tax + 1)
    end

    if @sale.deposits.count > 0
      @sale.payment_status = 'parcialmente_pagado'
    end
    @sale.accumulated = @sale.deposits.sum(:amount)
    @sale.remaining = @sale.total - @sale.accumulated

    if @sale.payment_status == 'venta_concretada'
      @sale.accumulated = @sale.total
      Deposit.create(amount: @sale.remaining, description: "Pago en una sola exhibición", sale_id: @sale.id)
    end
    if @sale.accumulated >= @sale.total 
      @sale.payment_status = 'venta_concretada'
    end

    respond_to do |format|
      if @sale.save
        format.html { redirect_to edit_admin_sale_path(@sale), notice: 'Sale was successfully updated.' }
        # format.json { render :show, status: :ok, location: @sale }
      else
        format.html { render :edit }
        # format.json { render json: @sale.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/sales/1
  # DELETE /admin/sales/1.json
  def destroy
    @sale.destroy
    respond_to do |format|
      format.html { redirect_to admin_sales_path, notice: 'Sale was successfully destroyed.' }
      # format.json { head :no_content }
    end
  end

  private
    def check_admin
      unless current_user.has_roles?([:super, :admin, :ventas, :gerente_ventas])
        redirect_to admin_root_path, notice: "Error 9001 no tienes permiso"
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_sale
      @sale = Sale.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sale_params
      params.require(:sale).permit(:creation, :created_at, :client_id, :desc, :discount, :subtotal, :tax, :total, :payment_type, :user_id, :payment_status, line_items_attributes: [:product_id, :quantity, :price, :_destroy, :id], deposits_attributes: [:amount, :description, :_destroy, :id])
    end
end
