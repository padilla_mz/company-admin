class Admin::ExpensesController < ApplicationController
  before_action :check_admin
  before_action :set_expense, only: [:show, :edit, :update, :destroy, :show_buy, :edit_buy, :update_buy]
  layout 'admin'

  ##### Extra methods
  # get 

  def index_nomina
    @q = Expense.ransack(params[:q])
    @expenses = @q.result(distinct: true).nominas
     respond_to do |f|
      f.html
      f.csv {headers['Content-Disposition'] = "attachment; filename=nominas_#{Time.now.strftime('%m-%d-%Y')}.csv"}

    end
  end

  def show_nomina
  end

  def new_nomina
    @expense = Expense.new
    @payment_types = Expense::Payment_forms_2
    # @tax = Expense::Tax_2
    @document_types = Expense::Documents
  end

  def edit_nomina
    
  end

  def create_nomina
    
  end

  def update_nomina
    
  end






  def index_buy
    @q = Expense.ransack(params[:q])
    @expenses = @q.result(distinct: true).expenses
     respond_to do |f|
      f.html
      f.csv {headers['Content-Disposition'] = "attachment; filename=gastos_#{Time.now.strftime('%m-%d-%Y')}.csv"}

    end
  end

  # get
  def new_buy
    @expense = Expense.new
    @payment_types = Expense::Payment_forms
    @tax = Expense::Tax_2
    @document_types = Expense::Documents
  end

  
  # get
  def edit_buy
    @payment_types = Expense::Payment_forms
    @tax = Expense::Tax_2
    @document_types = Expense::Documents

    
  end

  #get
  def show_buy
  end

  #POST
  def create_buy
    @expense = Expense.new(buy_params)
    @expense.user_id = current_user.id
    @expense.expense_type = 'expense'

    @expense.line_buys.each do |line|
      line.subtotal = line.price
      if line.tax
        line.total_price_line = (line.subtotal) * 1.16
      else
        line.total_price_line = line.subtotal
      end
      line.save
    end
    @expense.save
    @expense.total = @expense.line_buys.sum(:total_price_line)

    respond_to do |format|
      if @expense.save
        format.html { redirect_to index_buy_path, notice: 'Expense was successfully created.' }
        # format.json { render :show, status: :created, location: @expense }
      else
        format.html { render :new_buy }
        # format.json { render json: @admin_expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT PATCH
  def update_buy
    @expense.assign_attributes(buy_params)

    @expense.line_buys.each do |line|
      line.subtotal = line.price
      if line.tax
        line.total_price_line = (line.subtotal) * 1.16
      else
        line.total_price_line = line.subtotal
      end
      line.save
    end
    @expense.save
    @expense.total = @expense.line_buys.sum(:total_price_line)

    respond_to do |format|
      if @expense.save
        format.html { redirect_to edit_buy_path, notice: 'Expense was successfully created.' }
        # format.json { render :show, status: :created, location: @expense }
      else
        format.html { render :new_buy }
        # format.json { render json: @admin_expense.errors, status: :unprocessable_entity }
      end
    end
  end
  ##### .....



  # GET /admin/expenses
  # GET /admin/expenses.json
  def index
    @q = Expense.ransack(params[:q])
    @expenses = @q.result(distinct: true).purchases

    respond_to do |f|
      f.html
      f.csv {headers['Content-Disposition'] = "attachment; filename=compras_#{Time.now.strftime('%m-%d-%Y')}.csv"}

    end
  end

  # GET /admin/expenses/1
  # GET /admin/expenses/1.json
  def show
  end

  # GET /admin/expenses/new
  # New purchase
  def new
    @expense = Expense.new
    @expense_states  = Expense::State_of_payment
    @payment_types = Expense::Payment_forms
    @levels = Expense::Levels
    @tax = Expense::Tax
  end


  # GET /admin/expenses/1/edit
  def edit
    @expense_states  = Expense::State_of_payment
    @payment_types = Expense::Payment_forms
    @levels = Expense::Levels

    @tax = Expense::Tax


  end

  # POST /admin/expenses
  # POST /admin/expenses.json
  def create
    @expense = Expense.new(expense_params)
    @expense.user_id = current_user.id
    @expense.expense_type = 'purchase'
    @expense.line_expenses.each do |line|
      line.total_line_price = line.price * line.quantity
      line.save
    end
    @expense.save
    @expense.subtotal = @expense.line_expenses.sum(:total_line_price)

    if @expense.tax == 0
      @expense.total = @expense.subtotal
    else
      @expense.total = (@expense.subtotal) * (@expense.tax + 1)
    end

    respond_to do |format|
      if @expense.save
        format.html { redirect_to action: 'index', notice: 'Expense was successfully created.' }
        # format.json { render :show, status: :created, location: @expense }
      else
        format.html { render :new }
        # format.json { render json: @admin_expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/expenses/1
  # PATCH/PUT /admin/expenses/1.json
  def update
    @expense.assign_attributes(expense_params)
    # @expense.user_id = current_user.id
    @expense.line_expenses.each do |line|
      line.total_line_price = line.price * line.quantity
      line.save
    end
    @expense.save
    @expense.subtotal = @expense.line_expenses.sum(:total_line_price)

    if @expense.tax == 0
      @expense.total = @expense.subtotal
    else
      @expense.total = (@expense.subtotal) * (@expense.tax + 1)
    end

    respond_to do |format|
      if @expense.save
        format.html { redirect_to admin_expenses_path, notice: 'Expense was successfully updated.' }
        # format.json { render :show, status: :ok, location: @expense }
      else
        format.html { render :edit }
        # format.json { render json: @expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/expenses/1
  # DELETE /admin/expenses/1.json
  def destroy
    @expense.destroy
    respond_to do |format|
      format.html { redirect_to admin_expenses_path, notice: 'Expense was successfully destroyed.' }
      # format.json { head :no_content }
    end

  end

  private
    def check_admin
      unless current_user.has_roles?([:super, :admin, :compras, :gerente_compras])
        redirect_to admin_root_path, notice: "Error 9001 no tienes permiso"
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_expense
      @expense = Expense.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def expense_params
      params.require(:expense).permit(:creation, :tax, :level, :provider_id, :payment_status, :payment_type, :desc, line_expenses_attributes: [:provider_id, :product_id, :quantity, :price, :_destroy, :id])
    end
    def buy_params
      params.require(:expense).permit(:creation, :desc, line_buys_attributes: [:provider_id, :document_type, :document_sku, :desc, :quantity, :price, :tax, :payment_type, :_destroy, :id])
    end
    def nomina_params
      params.require(:expense).permit(:creation, :desc, line_buys_attributes: [:provider_id, :document_type, :document_sku, :desc, :quantity, :price, :tax, :payment_type, :_destroy, :id])
    end
end
