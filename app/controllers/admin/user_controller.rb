class Admin::UserController < ApplicationController
	# roles who aply here :super, :admin	
	# aqui solo se muestran los usuarios que manejan el admin y los roles
	before_action :check_admin
  before_action :set_user, only: [:edit, :update, :destroy]

	layout 'admin'

	# get
	def landing_users
	end

	#get 
	def index_users
		@q = User.employes(current_user).ransack(params[:q])
		@users = @q.result(distinct: true)
		respond_to do |f|
      f.html
      f.csv 
      f.pdf do
        pdf = ReportesPdf.new(@users)
        send_data(pdf.render, filename: "users.pdf", type: "application/pdf", disposition: "inline")
      end
    end
	end
	#get
	def web_users
		@q = User.web_users().ransack(params[:q])
		@users = @q.result(distinct: true)
		respond_to do |f|
      f.html
      f.csv 
      f.pdf do
        pdf = ReportesPdf.new(@users)
        send_data(pdf.render, filename: "web_users.pdf", type: "application/pdf", disposition: "inline")
      end
    end

	end

	# get
	def new
		@user = User.new
		@educations = User::Educations
		@roles = Role.where.not(name: ['super', 'cliente_web']).order(name: :asc)

	end

	#get
	def edit
		@educations = User::Educations
		@roles = Role.where.not(name: ['super', 'cliente_web']).order(name: :asc)
		

	end

	#post
	def create
		@user = User.new(user_params)
		respond_to do |f|
			if @user.save and @user.update(role_count: @user.roles.count)
				f.html { redirect_to user_edit_path(@user), notice: "Usuario creado"}
			else
				f.html { render :new }
			end
		end
	end

	# patch/put
	def update

		respond_to do |f|
			if @user.update(user_params) and @user.update(role_count: @user.roles.count)
				f.html { redirect_to user_edit_path(@user), notice: 'Usuario actualizado' }
			else
				f.html {render :edit}
			end
		end
	end

	# delete
	def destroy
		@user.destroy
		respond_to do |f|
			f. html {redirect_to users_list_path, notice: "Usuario eliminado con exito"}
		end
	end

	private 

		def check_admin
			unless current_user.has_roles?([:super, :admin])
				redirect_to admin_root_path, notice: "Error 9001 no tienes permiso"
			end
		end

		def set_user
			@user = User.find(params[:id])
		end
		def user_params
    	params.require(:user).permit(:name, :education, :rfc, :phone, :address, :col, :state, :ext_number, :int_number, :cp, :city, :last_name, :email, :password, :password_confirmation, role_ids:[])
    end
	
end
