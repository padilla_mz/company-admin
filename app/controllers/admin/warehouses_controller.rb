class Admin::WarehousesController < ApplicationController
  before_action :check_admin
  before_action :set_warehouse, only: [:show, :edit, :update, :destroy]
  layout 'admin'

  # GET /admin/warehouses
  # GET /admin/warehouses.json
  def index
    @q = Warehouse.ransack(params[:q])
    @warehouses = @q.result(distinct: true)
  end

  # GET /admin/warehouses/1
  # GET /admin/warehouses/1.json
  def show
  end

  # GET /admin/warehouses/new
  def new
    @warehouse = Warehouse.new
  end

  # GET /admin/warehouses/1/edit
  def edit
  end

  # POST /admin/warehouses
  # POST /admin/warehouses.json
  def create
    @warehouse = Warehouse.new(warehouse_params)

    respond_to do |format|
      if @warehouse.save
        format.html { redirect_to admin_warehouses_path, notice: 'Warehouse was successfully created.' }
        # format.json { render :show, status: :created, location: @warehouse }
      else
        format.html { render :new }
        # format.json { render json: @warehouse.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/warehouses/1
  # PATCH/PUT /admin/warehouses/1.json
  def update
    respond_to do |format|
      if @warehouse.update(warehouse_params)
        format.html { redirect_to admin_warehouses_path, notice: 'Warehouse was successfully updated.' }
        # format.json { render :show, status: :ok, location: @warehouse }
      else
        format.html { render :edit }
        # format.json { render json: @warehouse.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/warehouses/1
  # DELETE /admin/warehouses/1.json
  def destroy
    @warehouse.destroy
    respond_to do |format|
      format.html { redirect_to admin_warehouses_url, notice: 'Warehouse was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def check_admin
      unless current_user.has_roles?([:super, :admin, :gerente_inventario, :gerente_compras])
        redirect_to admin_root_path, notice: "Error 9001 no tienes permiso"
      end
    end
    # Use callbacks to share common setup or constraints between actions.
    def set_warehouse
      @warehouse = Warehouse.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def warehouse_params

      params.require(:warehouse).permit(:name, :address, :state, :city, :country, :cp, :phone)
    end
end
