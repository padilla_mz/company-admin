class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :authenticate_user!
  before_action :set_setting

  # def check_user_web
  #   if current_user.is?(:cliente_web)
  #     redirect_to root_path, notice: "tu no puedes estar aqui"
  #   end
  # end

  before_action :configure_permitted_parameters, if: :devise_controller?
  
  protected
  
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :last_name, :phone])
  end
  def set_setting
    @setting = Setting.first
  end
end
