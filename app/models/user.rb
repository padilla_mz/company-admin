class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_and_belongs_to_many :roles

  validates :email, uniqueness: true
  validates :name, :last_name, :email, presence: true
  
  scope :web_users, -> {where("role_count = ?", 0)}
  scope :employes, ->(user) {where("role_count > ?", 0).where.not(id: user)}

  def web_users
    where("roles_count < ?", 1).count
  end

  def roles_count
    count = self.roles.count.to_i
    return count
  end

  ROLES = %i[super admin  gerente_ventas ventas gerente_compras compras gerente_inventario inventario gerente_finanzas finanzas gerente_contabilidad contabilidad cliente_web]

  Educations = %i[doctorado maestria licenciado_titulado licenciado_pasante licenciado_trunca preparatoria preparatoria_trunca secundaria secundaria_trunca primaria]

  States = %i[]


  # def is?(requested_role)
  #   self.role == requested_role.to_s
  # end

  def has_role?(role)
    role = role.to_s
    self.roles.exists?(name: role)
  end

  def has_any_role?
    if self.roles.count > 0
      return true
    else
      return false
    end
  end

  def has_roles?(array_roles)
    input_roles = array_roles.map { |element| element.to_s }
    roles = self.roles.map { |rol| rol.name.to_s }
    
    collision = input_roles & roles
    !collision.empty?
  end

  def add_role(role)
    role = role.to_s
    ob = Role.find_by_name(role)

    if ob
      self.roles << ob
    else
      return false
    end
  end

  def delete_role(role)
    role = role.to_s
    ob = self.roles.find_by_name(role)
    if ob
      # ob.delete()
      self.roles.delete(ob)
    end
  end

  def delete_roles
    self.roles.delete(self.roles)
  end

  after_create :send_notification
  #
  def send_notification
    AdminMailer.welcome(self).deliver_now
    AdminMailer.new_user(self).deliver_now
  end

  before_save :set_role
  def set_role
    unless self.has_any_role?
      role = Role.find_by_name(:cliente_web).id
      user = self.id
      RolesUser.create(user_id: user, role_id: role )
    end
  end
end
