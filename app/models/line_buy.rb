class LineBuy < ApplicationRecord
  belongs_to :expense
  belongs_to :product
  belongs_to :provider


  def self.this_month
  	sum(:total_price_line)

  end
end
