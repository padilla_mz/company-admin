class Deposit < ApplicationRecord
  belongs_to :sale
  validates :amount, :description, presence: true

  scope :last_month, -> {where(created_at: (Date.today.beginning_of_month)..(Date.today.end_of_month))}
  scope :last_month_sum, -> {where(created_at: (Date.today.beginning_of_month)..(Date.today.end_of_month)).sum(:amount)}

end
