class Expense < ApplicationRecord
  belongs_to :provider
  belongs_to :user


  has_many :line_expenses
  has_many :products, through: :line_expenses

  has_many :paysheets
  has_many :users, through: :paysheets

  has_many :line_buys
  has_many :products, through: :line_buys

  accepts_nested_attributes_for :line_expenses, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :line_buys, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :paysheets, reject_if: :all_blank, allow_destroy: true

  scope :default, -> {order('creation desc')}

  scope :expenses, -> {default.where(expense_type: 'expense')}
  scope :purchases, -> {default.where(expense_type: 'purchase')}
  
  scope :nominas, -> {default.where(expense_type: 'nomina')}

  scope :last_month, -> {where(created_at: (Date.today.beginning_of_month)..(Date.today.end_of_month))}
  scope :last_month_sum, -> {where(creation: (Date.today.beginning_of_month)..(Date.today.end_of_month)).sum(:total)}



  validates :creation, presence: true

  def self.suma_compras
    # where(expense_type: 'expense').sum(:total) + where(expense_type: 'purchase').where(payment_status: 'compra_concretada').sum(:total)
    # where(expense_type: 'purchase')
    gastos = where(expense_type: 'expense').where(created_at: (Time.now - 1.month)..(Time.now)).sum(:total)
    compras = where(expense_type: 'purchase').where(payment_status: 'compra_concretada').where(created_at: (Time.now - 1.month)..(Time.now)).sum(:total)
    return gastos + compras
  end

  State_of_payment = [['Pendiente de pago', :pendiente_pago], ['Compra pagada', :compra_concretada]]
  Payment_forms = [['Contado', :contado], ['Cheque', :cheque], ['Transferencia bancaria', :deposito], ['Tajerta de crédito', :tarjeta_credito]]
  Payment_forms_2 = [['Contado', :contado], ['Cheque', :cheque], ['Transferencia bancaria', :deposito]]

  scope :default, -> {order('created_at desc')}

  # Levels of importance for a expense
  Levels = [['Nivel 3 (compra de poca importancia)',3],['Nivel 2 (compra de importancia media)',2],['Nivel 1 (compra de importancia alta)',1]]
  Tax = [['0 %', 0.0], ['16 %', 0.16]]
  Tax_2 = [['Si', true],['No',false]]
  Documents = [['Nota remision', :note], ['Ticket', :ticket],['Factura', :bill]]


end
