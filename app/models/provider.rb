class Provider < ApplicationRecord
	has_many :stock_entries
	# has_many :products
	# accepts_nested_attributes_for :stock_entries, reject_if: :all_blank, allow_destroy: true
	validates :razon, :rfc, :state, :city, :cp, :col, :address, :ext_number, :phone, :email, presence: true

end
