class Client < ApplicationRecord
	Limite = [0, 2000, 5000, 7000, 10000, 15000, 20000, 25000]

	validates :razon, :name, :rfc, :state, :city, :cp, :col, :address, :ext_number, :phone_1, :email, presence: true
	validates :razon, uniqueness: true
	# has_many :charges
end
