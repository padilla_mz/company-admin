class Entry < ApplicationRecord
	belongs_to :provider
	belongs_to :warehouse
	belongs_to :user
	belongs_to :client
	
	has_many :entries_products, dependent: :destroy
	has_many :products, through: :entries_products

	has_many :out_lines, dependent: :destroy
	has_many :products, through: :out_lines
	

	accepts_nested_attributes_for :entries_products, reject_if: :all_blank, allow_destroy: true
	accepts_nested_attributes_for :out_lines, reject_if: :all_blank, allow_destroy: true

	scope :default, -> {order('created_at desc')}

  scope :ins, -> {default.where(direction: :in)}
  scope :outs, -> {default.where(direction: :out)}


  Payment_forms = [['Crédito', :credito], ['Contado', :contado], ['Depósito', :deposito]]
  State_of_bills = [['Pendiente', :pendiente], ['Pagado', :pagado], ['Parcialmente pagado', :parcialmente_pagado]]
  Tax = [['0 %', 0], ['16 %', 0.16]]
  # Types = ['out', 'in']


	
end
