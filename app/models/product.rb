class Product < ApplicationRecord
  # Tipos = %i[llanta rin accesorio]
  Tipos = [['Materia prima', :materia_prima], ['Herramienta', :herramienta], ['Maquinaria', :maquinaria],['Producto terminado', :producto]]
  Unidades = [['Pieza', :pieza], ['Reja 12pzs', :reja_12], ['Reja 24pzs', :reja_24],['Caja', :caja], ['Litro', :l], ['Kg', :kg], ['Metro', :m],['Gramo', :gr], ['Mililitro', :ml]]

  has_many :entries_products
  has_many :entries, through: :entries_products



  has_many :line_expenses
  # has_many :expenses, through: :line_expenses

  has_many :line_buys
  # has_many :expenses, through: :line_buys

  belongs_to :provider

  validates :model, :public_price, :product_type, :sku, presence: true
  # validates :model, uniqueness: true

  def name_a
    "#{id} - #{model} (#{unidad})"
  end
  def name_d
    "#{id} - #{model} (#{unidad}) - ($ #{public_price})"
  end 

  def self.import(file)
    # CSV.foreach(file.path, headers: true) do |row|
    #     begin
    #       object = self.find(row[:id])
    #       if object.present?
    #         object.update!(row.to_hash)
    #       end
    #     rescue  
    #       self.create!(row.to_hash)
    #     end
    # end

    spreadsheet = Roo::Spreadsheet.open(file.path)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      product = find_by(id: row["id"]) || new
      product.attributes = row.to_hash
      # product.brand = product.brand.upcase
      product.model = product.model.upcase
      product.sku = "#{product.brand[0..2]}#{product.model[0..3]}#{product.ancho}-#{product.alto}-#{product.rin}"
      product.save!
    end
  end
end
