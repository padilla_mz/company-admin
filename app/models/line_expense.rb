class LineExpense < ApplicationRecord
  belongs_to :expense
  belongs_to :product
  belongs_to :provider

  def self.this_month
  	sum(:total_line_price)
  	# expense.payment_status
  end
end
