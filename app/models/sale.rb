class Sale < ApplicationRecord
  belongs_to :client
  belongs_to :user

  has_many :line_items, dependent: :destroy
  has_many :products, through: :line_items
  has_many :deposits, dependent: :destroy


  accepts_nested_attributes_for :line_items, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :deposits, reject_if: :all_blank, allow_destroy: true
  
  scope :default, -> {order('creation desc')}

  scope :cotizaciones, -> {default.where(payment_status: :cotizacion)}
  scope :por_pagar, -> {default.where(payment_status: :por_pagar)}
  scope :parcialmente_pagadas, -> {default.where(payment_status: :parcialmente_pagado)}
  # scope :por_cobrar, -> {default.where(payment_status: :cuenta_cobrar)}
  scope :pagadas, -> {default.where(payment_status: :venta_concretada)}



  Payment_forms = [['Contado', :contado], ['Cheque', :cheque], ['Depósito bancario, transferencia', :deposito], ['Tajerta de crédito, debito', :tarjeta_credito]]
  State_of_sale = [['Cotizacion', :cotizacion], ["Parcialmente pagado", :parcialmente_pagado], ['Por pagar(Pendiente de pago)', :por_pagar], ['Venta pagada', :venta_concretada]]
  # if the payment state is :cotizacion then
  State_of_sale_1 = [["Parcialmente pagado", :parcialmente_pagado], ['Por pagar(pendiente de pago)', :por_pagar], ['Venta pagada', :venta_concretada]]
  
  # if the payment state is :cuenta_cobrar or :cuenta_pagar then
  State_of_sale_2 = [['Por pagar(pendiente de pago)', :por_pagar], ["Parcialmente pagado", :parcialmente_pagado], ['Venta pagada', :venta_concretada]]

  # if the payment state is :parcialmente_pagado then
  State_of_sale_3 = [["Parcialmente pagado", :parcialmente_pagado], ['Venta pagada', :venta_concretada]]


  # if the payment state is :venta_concretada then
  State_of_sale_4 = [['Venta pagada', :venta_concretada]]

  
  # State_of_sale = [['Cotizacion', 1], ['Pendiente de pago(cuenta por pagar)', 2], ["Parcialmente pagado", 3], ['Cuenta por cobrar(liquidará dentro de 24hrs)', 4], ['Venta pagada', 5]]
  Tax = [['0 %', 0.0], ['16 %', 0.16]]
  Discount = [['0 %', 0.0], ['5 %', 0.05], ['7 %', 0.07], ['10 %', 0.1]]


end
