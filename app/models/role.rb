class Role < ApplicationRecord
  has_and_belongs_to_many :users

  validates :name, presence: true, uniqueness: true

  scope :roles_index, -> {where.not(name: :super)}

end
