class ProductosPdf < Prawn::Document
# reporte para productos

	def initialize(objects)
		super(top_margin: 70)
		@objects = objects
		@time = Time.new
		header
		body
	end
	
	def header
		move_up 30
		# image "#{Rails.root}/app/assets/images/logo-neon2.png", height: 80
		stroke_horizontal_rule
		move_down 20
		text "Reporte de inventario #{@time.strftime('%d-%m-%Y')}", size: 20, style: :bold
	end

	def body
		move_down 20
		# header = [["Title","Release date", "Uploaded date", "Company", "Serie", "Issue #", "User", "Stars"]]
		# array_2 = @comics.map do |item|
		# 	[item.title, item.release_date.strftime('%Y, %b %d'), item.created_at, item.company.name, item.serie.name, "##{item.issue_n}"]
		# end
		# @array = header + array_2

		table(items_rows) do
			self.header = true
			row(0).font_style = :bold
			self.cell_style = {size: 8}
			self.row_colors = ["DBDFE4", "FFFFFF"]
		end

		# table(items_rows, cell_style: (size:5), row_colors: ["F0F0F0", "FFFFFF"]) 
			
	end


	def items_rows
		header = [["ID","Nombre", "Unidad", "Cantidad", "Precio", "Precio Medio-mayoreo (5%)", "Precio Mayoreo (7%)"]]
		items = @objects.map do |item|
			[item.id, item.model, item.unidad, item.quantity, item.public_price, item.price_1, item.price_2]
		end
		return header + items
	end
end