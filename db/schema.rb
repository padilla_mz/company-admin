# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170123230535) do

  create_table "clients", force: :cascade do |t|
    t.string   "razon"
    t.string   "name"
    t.string   "rfc"
    t.string   "address"
    t.string   "col"
    t.string   "cp"
    t.string   "state"
    t.string   "city"
    t.string   "localidad"
    t.string   "phone_1"
    t.string   "phone_2"
    t.string   "ext_number"
    t.string   "int_number"
    t.string   "email"
    t.integer  "credit_limit"
    t.string   "tipo_venta"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "deposits", force: :cascade do |t|
    t.decimal  "amount"
    t.string   "description"
    t.integer  "sale_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["sale_id"], name: "index_deposits_on_sale_id"
  end

  create_table "entries", force: :cascade do |t|
    t.integer  "provider_id"
    t.integer  "warehouse_id"
    t.text     "comment"
    t.string   "number"
    t.string   "bill_number"
    t.string   "ticket_number"
    t.decimal  "subtotal",      precision: 8, scale: 2
    t.decimal  "tax",           precision: 8, scale: 2
    t.decimal  "total",         precision: 8, scale: 2
    t.string   "payment_form"
    t.string   "state_of_bill"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.integer  "user_id"
    t.date     "received_at"
    t.date     "payment_date"
    t.integer  "client_id"
    t.string   "direction"
    t.index ["client_id"], name: "index_entries_on_client_id"
    t.index ["provider_id"], name: "index_entries_on_provider_id"
    t.index ["user_id"], name: "index_entries_on_user_id"
    t.index ["warehouse_id"], name: "index_entries_on_warehouse_id"
  end

  create_table "entries_products", force: :cascade do |t|
    t.integer  "entry_id"
    t.integer  "product_id"
    t.integer  "quantity"
    t.decimal  "price",            precision: 8, scale: 2
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "entry_sku"
    t.decimal  "total_price_line", precision: 8, scale: 2
    t.index ["entry_id"], name: "index_entries_products_on_entry_id"
    t.index ["product_id"], name: "index_entries_products_on_product_id"
  end

  create_table "expenses", force: :cascade do |t|
    t.text     "desc"
    t.integer  "provider_id"
    t.integer  "user_id"
    t.string   "payment_type"
    t.string   "payment_status"
    t.decimal  "subtotal",       precision: 8, scale: 2
    t.decimal  "tax",            precision: 8, scale: 2
    t.decimal  "total",          precision: 8, scale: 2
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.integer  "level",                                  default: 3
    t.string   "expense_type"
    t.date     "creation"
    t.index ["provider_id"], name: "index_expenses_on_provider_id"
    t.index ["user_id"], name: "index_expenses_on_user_id"
  end

  create_table "line_buys", force: :cascade do |t|
    t.decimal  "price",            precision: 8, scale: 2
    t.integer  "quantity"
    t.string   "desc"
    t.integer  "expense_id"
    t.integer  "product_id"
    t.decimal  "total_price_line", precision: 8, scale: 2
    t.integer  "provider_id"
    t.boolean  "tax"
    t.decimal  "subtotal",         precision: 8, scale: 2
    t.string   "payment_type"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "document_type"
    t.string   "document_sku"
    t.index ["expense_id"], name: "index_line_buys_on_expense_id"
    t.index ["product_id"], name: "index_line_buys_on_product_id"
    t.index ["provider_id"], name: "index_line_buys_on_provider_id"
  end

  create_table "line_expenses", force: :cascade do |t|
    t.decimal  "price",            precision: 8, scale: 2
    t.integer  "quantity"
    t.string   "desc"
    t.integer  "expense_id"
    t.integer  "product_id"
    t.decimal  "total_line_price", precision: 8, scale: 2
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "provider_id"
    t.index ["expense_id"], name: "index_line_expenses_on_expense_id"
    t.index ["product_id"], name: "index_line_expenses_on_product_id"
    t.index ["provider_id"], name: "index_line_expenses_on_provider_id"
  end

  create_table "line_items", force: :cascade do |t|
    t.integer  "sale_id"
    t.integer  "product_id"
    t.integer  "quantity"
    t.decimal  "price",            precision: 8, scale: 2
    t.decimal  "total_price_line", precision: 8, scale: 2
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.index ["product_id"], name: "index_line_items_on_product_id"
    t.index ["sale_id"], name: "index_line_items_on_sale_id"
  end

  create_table "out_lines", force: :cascade do |t|
    t.integer  "entry_id"
    t.integer  "product_id"
    t.integer  "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["entry_id"], name: "index_out_lines_on_entry_id"
    t.index ["product_id"], name: "index_out_lines_on_product_id"
  end

  create_table "paysheets", force: :cascade do |t|
    t.decimal  "amount",        precision: 8, scale: 2
    t.text     "desc"
    t.integer  "user_id"
    t.integer  "expense_id"
    t.decimal  "tax"
    t.string   "payment_type"
    t.string   "document_type"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.datetime "from_date"
    t.datetime "to_date"
    t.date     "payment_date"
    t.decimal  "worked_days",   precision: 8, scale: 2
    t.index ["expense_id"], name: "index_paysheets_on_expense_id"
    t.index ["user_id"], name: "index_paysheets_on_user_id"
  end

  create_table "products", force: :cascade do |t|
    t.string   "product_type"
    t.string   "sku"
    t.string   "brand"
    t.string   "model"
    t.string   "ancho"
    t.string   "alto"
    t.string   "rin"
    t.string   "ind_carga"
    t.string   "ind_velocidad"
    t.datetime "created_at",                                        null: false
    t.datetime "updated_at",                                        null: false
    t.integer  "treadwear"
    t.text     "desc"
    t.integer  "quantity",                              default: 0
    t.decimal  "cost",          precision: 8, scale: 2
    t.decimal  "price_1",       precision: 8, scale: 2
    t.decimal  "price_2",       precision: 8, scale: 2
    t.decimal  "public_price",  precision: 8, scale: 2
    t.string   "unidad"
    t.integer  "capacity"
    t.integer  "provider_id"
    t.index ["provider_id"], name: "index_products_on_provider_id"
  end

  create_table "providers", force: :cascade do |t|
    t.string   "razon"
    t.string   "rfc"
    t.string   "address"
    t.string   "cp"
    t.string   "city"
    t.string   "localidad"
    t.string   "ext_number"
    t.string   "int_number"
    t.string   "phone"
    t.string   "email"
    t.string   "state"
    t.string   "col"
    t.string   "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.text     "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles_users", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["role_id"], name: "index_roles_users_on_role_id"
    t.index ["user_id"], name: "index_roles_users_on_user_id"
  end

  create_table "sales", force: :cascade do |t|
    t.integer  "client_id"
    t.decimal  "subtotal",       precision: 8, scale: 2
    t.decimal  "tax",            precision: 8, scale: 2
    t.decimal  "total",          precision: 8, scale: 2
    t.string   "payment_type"
    t.integer  "user_id"
    t.string   "payment_status"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.string   "sku"
    t.decimal  "discount",       precision: 8, scale: 2,                 null: false
    t.text     "desc"
    t.decimal  "accumulated",    precision: 8, scale: 2, default: "0.0"
    t.date     "creation"
    t.decimal  "remaining",      precision: 8, scale: 2
    t.index ["client_id"], name: "index_sales_on_client_id"
    t.index ["user_id"], name: "index_sales_on_user_id"
  end

  create_table "settings", force: :cascade do |t|
    t.text     "img_64"
    t.string   "name"
    t.string   "phone_1"
    t.string   "phone_2"
    t.string   "email_1"
    t.string   "email_2"
    t.string   "web_page"
    t.string   "address"
    t.string   "state"
    t.string   "city"
    t.string   "cp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "razon"
    t.string   "rfc"
    t.string   "ext_number"
    t.string   "int_number"
    t.string   "col"
    t.string   "localidad"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "last_name"
    t.string   "phone"
    t.string   "email",                                          default: "", null: false
    t.string   "encrypted_password",                             default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                  default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                                                  null: false
    t.datetime "updated_at",                                                  null: false
    t.string   "address"
    t.string   "col"
    t.string   "state"
    t.string   "ext_number"
    t.string   "int_number"
    t.string   "rfc"
    t.integer  "cp"
    t.string   "city"
    t.string   "education"
    t.integer  "role_count",                                     default: 0
    t.string   "area"
    t.date     "entry_date"
    t.string   "job"
    t.string   "imss_number"
    t.decimal  "salary",                 precision: 8, scale: 2
    t.decimal  "despensa",               precision: 8, scale: 2
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "warehouses", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.string   "state"
    t.string   "city"
    t.string   "country"
    t.string   "cp"
    t.string   "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
