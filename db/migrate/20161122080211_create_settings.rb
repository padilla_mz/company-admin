class CreateSettings < ActiveRecord::Migration[5.0]
  def change
    create_table :settings do |t|
      t.text :img_64
      t.string :name
      t.string :phone_1
      t.string :phone_2
      t.string :email_1
      t.string :email_2
      t.string :web_page
      t.string :address
      t.string :state
      t.string :city
      t.string :cp

      t.timestamps
    end
  end
end
