class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :product_type
      t.string :sku
      t.string :brand
      t.string :model
      t.string :ancho
      t.string :alto
      t.string :rin
      t.integer :treadwear
      t.string :ind_carga
      t.string :ind_velocidad
      t.integer :price_publico
      t.integer :price_mayoreo
      t.integer :price_especial
      t.integer :price_tarjeta
      t.integer :price_mercadopago
      t.integer :price_costo
      t.text :desc

      t.timestamps
    end
  end
end
