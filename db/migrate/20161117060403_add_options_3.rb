class AddOptions3 < ActiveRecord::Migration[5.0]
  def change
  	add_column :entries, :received_at, :date
  	add_column :entries, :payment_date, :date
  	add_column :entries_products, :entry_sku, :string

  end
end
