class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients do |t|
      t.string :razon
      t.string :name
      t.string :rfc
      t.string :address
      t.string :col
      t.string :cp
      t.string :state
      t.string :city
      t.string :localidad
      t.string :phone_1
      t.string :phone_2
      t.string :ext_number
      t.string :int_number
      t.string :email
      t.integer :credit_limit
      t.string :tipo_venta

      t.timestamps
    end
  end
end
