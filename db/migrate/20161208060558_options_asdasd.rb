class OptionsAsdasd < ActiveRecord::Migration[5.0]
  def change
  	add_column :entries, :direction, :string
  	remove_column :entries, :type, :string
  end
end
