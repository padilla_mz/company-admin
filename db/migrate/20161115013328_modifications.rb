class Modifications < ActiveRecord::Migration[5.0]
  def change
  	remove_column :products, :price_publico
  	remove_column :products, :price_mayoreo
  	remove_column :products, :price_especial
  	remove_column :products, :price_tarjeta
  	remove_column :products, :price_mercadopago
  	remove_column :products, :price_costo

  	add_column :products, :cost, :decimal, precision: 8, scale: 2
  	add_column :products, :price_1, :decimal, precision: 8, scale: 2
  	add_column :products, :price_2, :decimal, precision: 8, scale: 2
  	add_column :products, :price_3, :decimal, precision: 8, scale: 2
  	add_column :products, :public_price, :decimal, precision: 8, scale: 2
  end
end
