class AddOptions6 < ActiveRecord::Migration[5.0]
  def change
  	add_column :sales, :discount, :decimal, precision: 8, scale: 2
  end
end
