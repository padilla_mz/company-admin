class CreateWarehouses < ActiveRecord::Migration[5.0]
  def change
    create_table :warehouses do |t|
      t.string :name
      t.string :address
      t.string :state
      t.string :city
      t.string :country
      t.string :cp
      t.string :phone

      t.timestamps
    end
  end
end
