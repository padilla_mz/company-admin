class CreateEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :entries do |t|
      t.references :provider, foreign_key: true
      t.references :warehouse, foreign_key: true
      t.text :comment
      t.string :number
      t.string :bill_number
      t.string :ticket_number

      t.decimal :subtotal, precision: 8, scale: 2
      t.decimal :tax, precision: 8, scale: 2
      t.decimal :total, precision: 8, scale: 2

      t.string :payment_form
      t.string :state_of_bill

      t.timestamps
    end
  end
end
