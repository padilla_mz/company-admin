class AddOptions4 < ActiveRecord::Migration[5.0]
  def change
  	add_column :entries_products, :total_price_line, :decimal, precision: 8, scale: 2
  end
end
