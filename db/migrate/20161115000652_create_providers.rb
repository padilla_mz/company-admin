class CreateProviders < ActiveRecord::Migration[5.0]
  def change
    create_table :providers do |t|
      t.string :razon
      t.string :rfc
      t.string :address
      t.string :cp
      t.string :city
      t.string :localidad
      t.string :ext_number
      t.string :int_number
      t.string :phone
      t.string :email
      t.string :state
      t.string :col
      t.string :comment

      t.timestamps
    end
  end
end
