class CreateLineBuys < ActiveRecord::Migration[5.0]
  def change
    create_table :line_buys do |t|
      t.decimal :price, precision: 8, scale: 2
      t.integer :quantity
      t.string :desc
      t.references :expense, foreign_key: true
      t.references :product, foreign_key: true
      t.decimal :total_price_line, precision: 8, scale: 2
      t.references :provider, foreign_key: true
      t.boolean :tax
      t.decimal :subtotal, precision: 8, scale: 2
      t.string :payment_type

      t.timestamps
    end
  end
end
