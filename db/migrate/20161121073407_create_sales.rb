class CreateSales < ActiveRecord::Migration[5.0]
  def change
    create_table :sales do |t|
      t.references :client, foreign_key: true
      t.decimal :subtotal, precision: 8, scale: 2
      t.decimal :tax, precision: 8, scale: 2
      t.decimal :total, precision: 8, scale: 2
      t.string :payment_type
      t.references :user, foreign_key: true
      t.string :payment_status

      t.timestamps
    end
  end
end
