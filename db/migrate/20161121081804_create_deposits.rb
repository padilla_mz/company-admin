class CreateDeposits < ActiveRecord::Migration[5.0]
  def change
    create_table :deposits do |t|
      t.decimal :amount
      t.string :description
      t.references :sale, foreign_key: true

      t.timestamps
    end
  end
end
