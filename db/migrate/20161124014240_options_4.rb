class Options4 < ActiveRecord::Migration[5.0]
  def change
  	change_column :sales, :discount, :decimal, precision: 8, scale: 2, null: false
  end
end
