class OptionsAosidj8 < ActiveRecord::Migration[5.0]
  def change
  	add_column :settings, :razon, :string
  	add_column :settings, :rfc, :string
  	add_column :settings, :ext_number, :string
  	add_column :settings, :int_number, :string
  	add_column :settings, :col, :string
  	add_column :settings, :localidad, :string

  end
end
