class DeleteStuffFromUsers < ActiveRecord::Migration[5.0]
  def change
  	remove_column :users, :phone_number
  	remove_column :users, :roles_mask
  	remove_column :users, :role
  end
end
