class CreateLineItems < ActiveRecord::Migration[5.0]
  def change
    create_table :line_items do |t|
      t.references :sale, foreign_key: true
      t.references :product, foreign_key: true
      t.integer :quantity
      t.decimal :price, precision: 8, scale: 2
      t.decimal :total_price_line, precision: 8, scale: 2

      t.timestamps
    end
  end
end
