class CreateOutLines < ActiveRecord::Migration[5.0]
  def change
    create_table :out_lines do |t|
      t.references :entry, foreign_key: true
      t.references :product, foreign_key: true
      t.integer :quantity

      t.timestamps
    end
  end
end
