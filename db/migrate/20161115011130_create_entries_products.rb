class CreateEntriesProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :entries_products do |t|
      t.references :entry, foreign_key: true
      t.references :product, foreign_key: true

      t.integer :quantity
      t.decimal :price, precision: 8, scale: 2

      t.timestamps
    end
  end
end
