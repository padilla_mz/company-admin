class AddOptions1239487 < ActiveRecord::Migration[5.0]
  def change
  	add_column :sales, :remaining, :decimal, precision: 8, scale: 2
  end
end
