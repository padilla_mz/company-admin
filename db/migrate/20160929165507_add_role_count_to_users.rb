class AddRoleCountToUsers < ActiveRecord::Migration[5.0]
  def change
  	add_column :users, :role_count, :integer, default: 0
  end
end
