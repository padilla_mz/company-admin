class CreateLineExpenses < ActiveRecord::Migration[5.0]
  def change
    create_table :line_expenses do |t|
      t.decimal :price, precision: 8, scale: 2
      t.integer :quantity
      t.string :desc
      t.references :expense, foreign_key: true
      t.references :product, foreign_key: true
      t.decimal :total_line_price, precision: 8, scale: 2

      t.timestamps
    end
  end
end
