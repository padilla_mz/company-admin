class AddMoreFieldsToUser < ActiveRecord::Migration[5.0]
  def change
    change_column :users, :role, :string, default: "cliente_web"
    add_column :users, :address, :string
    add_column :users, :col, :string
    add_column :users, :state, :string
    add_column :users, :phone_number, :string
    add_column :users, :ext_number, :string
    add_column :users, :int_number, :string
    add_column :users, :rfc, :string
    add_column :users, :cp, :integer
    add_column :users, :city, :string
    add_column :users, :education, :string

  end
end
