class AddingMoreFieldsToPaysheets < ActiveRecord::Migration[5.0]
  def change
  	add_column :paysheets, :from_date, :datetime
  	add_column :paysheets, :to_date, :datetime
  	add_column :paysheets, :payment_date, :date
  	add_column :paysheets, :worked_days, :decimal, precision: 8, scale: 2

  	add_column :users, :area, :string
  	add_column :users, :entry_date, :date
  	add_column :users, :job, :string
  	add_column :users, :imss_number, :string
  	add_column :users, :salary, :decimal, precision: 8, scale: 2
  	add_column :users, :despensa, :decimal, precision: 8, scale: 2
  end
end
