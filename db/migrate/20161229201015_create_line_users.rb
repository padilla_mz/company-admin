class CreateLineUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :line_users do |t|
      t.decimal :amount, precision: 8, scale: 2
      t.text :desc
      t.references :user, foreign_key: true
      t.references :expense, foreign_key: true
      t.decimal :tax
      t.string :payment_type
      t.string :document_type

      t.timestamps
    end
  end
end
