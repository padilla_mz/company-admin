# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

roles = Role.create([
    {name: "super", comment: ""},
    {name: "admin", comment: ""},
    {name: "gerente_ventas", comment: ""},
    {name: "ventas", comment: ""},
    {name: "gerente_compras", comment: ""},
    {name: "compras", comment: ""},
    {name: "gerente_inventario", comment: ""},
    {name: "inventario", comment: ""},
    {name: "gerente_finanzas", comment: ""},
    {name: "finanzas", comment: ""},
    {name: "gerente_contabilidad", comment: ""},
    {name: "contabilidad", comment: ""},
    {name: "produccion", comment: ""},
    {name: "gerente_produccion", comment: ""},
    {name: "cliente_web", comment: ""},
  ])

company = Setting.create(
    img_64: "",
    name: "Company name",
    phone_1: "3481012211",
    phone_1: "3481012211",
    email_1: "info@example.com",
    email_2: "info@example.com",
    web_page: "www.example.com",
    address: "Palo Alto 8990",
    state: "California",
    city: "San Francisco",
    cp: "48900"
  )

  angel = User.create(
    email: "angelpadillam@gmail.com",
    name: "Angel",
    last_name: "Padilla",
  	password: "19891001aaa",
  	password_confirmation: "19891001aaa",
  )
  role = Role.find_by_name("super")

  superol = RolesUser.create(
    user_id: angel.id,
    role_id: role.id
    )
