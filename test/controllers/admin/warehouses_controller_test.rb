require 'test_helper'

class Admin::WarehousesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @admin_warehouse = admin_warehouses(:one)
  end

  test "should get index" do
    get admin_warehouses_url
    assert_response :success
  end

  test "should get new" do
    get new_admin_warehouse_url
    assert_response :success
  end

  test "should create admin_warehouse" do
    assert_difference('Admin::Warehouse.count') do
      post admin_warehouses_url, params: { admin_warehouse: {  } }
    end

    assert_redirected_to admin_warehouse_url(Admin::Warehouse.last)
  end

  test "should show admin_warehouse" do
    get admin_warehouse_url(@admin_warehouse)
    assert_response :success
  end

  test "should get edit" do
    get edit_admin_warehouse_url(@admin_warehouse)
    assert_response :success
  end

  test "should update admin_warehouse" do
    patch admin_warehouse_url(@admin_warehouse), params: { admin_warehouse: {  } }
    assert_redirected_to admin_warehouse_url(@admin_warehouse)
  end

  test "should destroy admin_warehouse" do
    assert_difference('Admin::Warehouse.count', -1) do
      delete admin_warehouse_url(@admin_warehouse)
    end

    assert_redirected_to admin_warehouses_url
  end
end
